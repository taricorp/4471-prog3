# export CFLAGS="-DDEBUG" for debug output
# Also recommended: -Wall -Wextra
CFLAGS := -g -std=c99 $(CFLAGS)
CC := gcc

.PHONY: all clean
all: sume

clean:
	rm -f sume

sume: sume.c


#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#ifdef DEBUG
# define dprint(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)
#else
# define dprint(...) ((void)0)
#endif

#define FAIL(s) do { *errorStr = s; return 0; } while (0)

uid_t guestUID, hostUID;
#define SU() seteuid(hostUID)
#define DROP() seteuid(guestUID)

// Drop EUID to guest's RUID
// Clear environment
// Find host's home directory
// Validate requested command
//      Must be one of 'cat' or 'ls'
//      If requested command is 'ls'
//          Ensure no options other than -s or -m are specified
//      Ensure requested file(s) are under ${HOME} of the host user (use realpath())
//          Specifying no files is legal
// Find config file of host user ${HOME}/.sumeCfg
// Validate guest in host's config
//      Elevate back to host EUID
//      Ensure config file is owned by host and neither group nor world write is allowed
//      Open config file
//      Drop EUID back to guest
//      Parse config file, ensure guest is in it
//      Close config file
// fork
// In child:
//      Elevate back to host EUID
//      exec() /bin/ls or /bin/cat as specified
// In parent:
//      Wait for child to exit
//      exit with return code from child
const char *const CONFIG_NAME = ".sumeCfg";

/*
 * Resolves path to its canonical version, returning a string.
 * Returned string must be free()d by the caller.
 */
char *abspath(const char *relpath) {
    // canonicalize_file_name() tries to resolve symlinks, so we want to be the
    // host user when invoking it.
    SU(); //Increase permissions so that canonicalize_file_name() can work
    char *abs = canonicalize_file_name(relpath);
    DROP(); //Drop permissions.
    dprint("Canonical target is %s\n", abs);
    return abs;
}

int isLegalCommand(char **const args, int nargs,
                   const char *hostHomeDir,
                   const char **errorStr) {
    if(nargs < 1) { *errorStr = "Not enough arguments provided"; return 0; }
    if(strcmp(args[0],"ls")  != 0 && strcmp(args[0], "cat") != 0){
        FAIL("Only `ls` and `cat` are permitted.");
    }
    if(strcmp(args[0],"ls") == 0){
        int seenPath = 0;
        for(int a = 1; a< nargs; a +=1){
            if(args[a][0] == '-'){
                if (seenPath == 1 ) { FAIL("Options not permitted after file"); }
                if( strlen(args[a] )!= 2 ) { FAIL("Invalid option"); }
                if(args[a][1] != 's' && args[a][1] != 'm') { FAIL("Only -s and -m are permitted"); }
            } else {
                seenPath = 1;
                char *fullPath = abspath(args[a]);
                if(fullPath == NULL) {
                    FAIL(NULL);
                }
                if(strncmp(hostHomeDir, fullPath, strlen(hostHomeDir)) != 0){
                    free(fullPath);
                    FAIL("File is not in home directory");
                }
                free(fullPath);
            }
        }
        //no errors found, this ls command is valid.
        return 1;
    } else if (strcmp(args[0],"cat") == 0){
        for(int a = 1; a< nargs; a +=1){
            if(args[a][0] != '-'){
                //checking path.
                char *fullPath = abspath(args[a]);
                if (fullPath == NULL) {
                    FAIL(NULL);
                }
                if(strncmp(hostHomeDir,fullPath,strlen(hostHomeDir)) != 0){
                    FAIL("File is not in home directory");
                }
            }
        }
        //no errors on cat command
        return 1;
    }
    
    FAIL("Something went terribly wrong");
}

int isGuestAllowed(const char *guestName,
                   const char *hostConfigPath,
                   uid_t hostUID, uid_t guestUID,
                   const char **errorStr) {
    // Change UID to host, open config file, drop perms again
    SU();
    int fd = open(hostConfigPath, O_RDONLY);
    DROP();
    if (fd == -1) FAIL("Unable to open host config file");

    // Check permissions on host config file
    struct stat st;
    if (fstat(fd, &st) == -1) {
        close(fd);
        FAIL(NULL);
    }
    if (st.st_uid != hostUID || (st.st_mode & 022) != 0 ) {
        close(fd);
        FAIL("Host config must be owned by host and writable only by owner");
    }

    // Map in file contents
    const char *mm = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mm == MAP_FAILED) {
        close(fd);
        FAIL(NULL);
    }

    int out = 0;
    off_t i = 0;
    while (i < st.st_size) {
        // Skip leading whitespace
        while (isspace(mm[i]) && i < st.st_size)
            i++;
        // Check substring
        size_t ofs;
        for (ofs = 0; ofs < strlen(guestName) && i < st.st_size; ofs++) {
            if (mm[i++] != guestName[ofs]) {
                break;
            }
        }
        if (ofs == strlen(guestName) && (i == st.st_size || isspace(mm[i]))) {
            dprint("Found %s in %s at offset %u\n", guestName, hostConfigPath, (unsigned)(i - ofs));
            out = 1;
            break;
        }
        // Skip word
        while (!isspace(mm[i]) && i < st.st_size)
            i++;
    }

    // Clean up and finish
    munmap((void *)mm, st.st_size);
    close(fd);
    if (out == 0) *errorStr = "Not authorized by host";
    return out;
}

void lerror(const char *msg, const char *reason) {
    if (reason == NULL) {
        if (errno != 0) {
            reason = strerror(errno);
        } else {
            reason = "unspecified error";
        }
    }
    fprintf(stderr, "%s: %s\n", msg, reason);
    exit(1);
}

int main(int argc, char **argv) {
    uid_t suid;
    getresuid(&guestUID, &hostUID, &suid); // Only fails on wrong parameters
    dprint("Running with host UID %u and guest %u\n", (unsigned)hostUID, (unsigned)guestUID);

    // Drop to guest permissions
    DROP();

    // Clear environment for security
    clearenv();

    // Find host's home directory
    struct passwd *pwent = getpwuid(guestUID);
    const char *guestName = NULL;
    if (pwent != NULL)
        guestName = strdup(pwent->pw_name);
    pwent = getpwuid(hostUID);
    if (guestName == NULL || pwent == NULL) {
        perror("getpwuid");
        return 1;
    }
    const char *const hostHomeDir = pwent->pw_dir;
    dprint("Guest's name is %s, host's home is %s\n", guestName, hostHomeDir);

    // Ensure requested command is legal
    const char *localError = NULL;
    if (!isLegalCommand(&argv[1], argc - 1, hostHomeDir, &localError)) {
        lerror("Illegal command", localError);
    }

    // Find host's .sumeCfg
    size_t hostConfigLen = strlen(hostHomeDir) + strlen(CONFIG_NAME) + 2;
    char *hostConfigPath = malloc(hostConfigLen);
    if (hostConfigPath == NULL) {
        perror("hostConfigPath");
        return 1;
    }
    snprintf(hostConfigPath, hostConfigLen, "%s/%s", hostHomeDir, CONFIG_NAME);
    dprint("Host's config file is at %s\n", hostConfigPath);

    // Validate host's config against current user
    if (!isGuestAllowed(guestName, hostConfigPath, hostUID, guestUID, &localError)) {
        lerror("You are not allowed to use this program", localError);
    }

    // Execute requested program
    char **c_argv = malloc(argc * sizeof(char *));
    if (c_argv != NULL) {
        // Last arg must be NULL to mark end
        c_argv[argc - 1] = NULL;
        // Determine program name
        size_t c_pathLen = strlen(argv[1]) + strlen("/bin/") + 1;
        char *c_path = malloc(c_pathLen);
        if (c_path != NULL) {
            snprintf(c_path, c_pathLen, "/bin/%s", argv[1]);
            dprint("Will execute %s\n", c_path);
            
            c_argv[0] = c_path;
            for (int i = 1; i < argc; i++) {
                c_argv[i] = argv[i + 1];
            }

            for (int i = 0; i < argc; i++) {
                dprint("\t%s\n", c_argv[i]);
            }
            // Re-elevate permissions and exec
            SU();
            execv(c_path, c_argv);
            DROP();
        }
    }
    //if we got here exec failed.
    perror("exec");
    exit(1);
}
